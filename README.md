# Nexylan templates docs

Global documentation concerning all projects under the nexylan/templates group.

## Deployment

All our templates which support deployment use docker and nexylan/ci.

Looks at the following section and the template specific configurations for more information.

### CI Runner

```
docker run --detach \
  --name gitlab-runner \
  --restart always \
  --volume gitlab-runner:/etc/gitlab-runner \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
docker exec -it gitlab-runner bash
export GITLAB_URL=https://gitlab.com/
export GITLAB_TOKEN=REGISTRATION_TOKEN
gitlab-runner \
    register \
    --name "Docker deploy" \
    --executor docker \
    --docker-image docker:latest \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN \
    --tag-list deploy:docker
```

The `REGISTRATION_TOKEN` can be found on your project or group CI/CD settings page.

More information: https://docs.gitlab.com/runner/install/

### Router

The project is designed to work out of the box thanks to Traefik.

To setup Traefik with ease, please see this project: https://gitlab.com/nexylan/docker/router

### Project

The project docker stack is automatically deployed on the right environnement thanks to the Nexylan CI auto devops system.

Just create a new branch and submit a Merge Request to see it in action.

For any documentation or troubleshooting: https://gitlab.com/nexylan/ci/
